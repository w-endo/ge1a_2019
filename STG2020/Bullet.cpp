#include "Bullet.h"
#include "Engine/Model.h"
#include "Collider.h"

//コンストラクタ
Bullet::Bullet(GameObject * parent)
	:GameObject(parent, "Bullet")
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Assets\\Model\\Oden.fbx");
	assert(hModel_ >= 0);

	transform_.scale_.vecX = 0.2f;
	transform_.scale_.vecY = 0.2f;
	transform_.scale_.vecZ = 0.2f;
	transform_.rotate_.vecX = 90.0f;

	Collider* collider = new Collider(XMVectorSet(0, 0, 0, 0), 0.5f);
	AddCollider(collider);
}

//更新
void Bullet::Update()
{
	transform_.position_.vecZ += 0.3f;

	if (transform_.position_.vecZ > 30)
	{
		KillMe();
	}
}

//描画
void Bullet::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}