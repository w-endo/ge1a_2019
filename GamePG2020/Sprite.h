#pragma once
#include <DirectXMath.h>
#include <string>
#include "Direct3D.h"
#include "Texture.h"
#include "Transform.h"

using namespace DirectX;




class Sprite
{

	//コンスタントバッファー
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matW;
	};

	//頂点情報
	struct VERTEX
	{
		XMVECTOR position;
		XMVECTOR uv;
	};


protected:
	ID3D11Buffer *pVertexBuffer_;
	ID3D11Buffer *pIndexBuffer_;
	ID3D11Buffer *pConstantBuffer_;
	Texture*	pTexture_;
	int indexNum;	//インデックス情報の数

public:
	Sprite();
	~Sprite();
	virtual HRESULT Initialize(std:: string fileName);
	void Draw(Transform& transform);
	void Release();
};

