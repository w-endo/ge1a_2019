#pragma once
#include <DirectXMath.h>
using namespace DirectX;

class GameObject;
class Collider
{
	XMVECTOR	center_;
	float		radius_;
	GameObject*	pMaster_;	//このコライダーを付けたオブジェクト

public:
	Collider(XMVECTOR center, float radius);
	bool Collision(Collider* pTarget);
	void SetMaster(GameObject*	master)
	{
		pMaster_ = master;
	}
};

