#include "Model.h"

namespace Model
{
	std::vector<ModelData*>	datas;

	int Load(std::string fileName)
	{
		ModelData* pData = new ModelData;
		pData->fileName = fileName;

		for (int i = 0; i < datas.size(); i++)
		{
			if (datas[i]->fileName == fileName)
			{
				pData->pFbx = datas[i]->pFbx;
				break;
			}
		}

		if (pData->pFbx == nullptr)
		{
			pData->pFbx = new Fbx;
			pData->pFbx->Load(fileName.c_str());
		}

		datas.push_back(pData);
		return datas.size() - 1;
	}

	void SetTransform(int handle, Transform & transform)
	{
		datas[handle]->transform = transform;
	}

	void Draw(int handle)
	{
		datas[handle]->pFbx->Draw(datas[handle]->transform);
	}
}