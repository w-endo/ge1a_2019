#pragma once
#include "GameObject.h"

//ゲームに登場するシーン
enum SCENE_ID
{
	SCENE_ID_PLAY = 0,
	SCENE_ID_TEST,
};


class SceneManager : public GameObject
{
	SCENE_ID nowScene_, nextScene_;
public:
	SceneManager();
	SceneManager(GameObject* parent);
	~SceneManager();

	// GameObject を介して継承されました
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;


	void ChangeScene(SCENE_ID scene);
};

