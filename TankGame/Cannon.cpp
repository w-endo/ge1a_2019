#include "Cannon.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Bullet.h"

//コンストラクタ
Cannon::Cannon(GameObject * parent)
	:GameObject(parent, "Cannon"), hModel_(-1),
	SWING_SPEED(3.0f)
{

}

//デストラクタ
Cannon::~Cannon()
{
}

//初期化
void Cannon::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("TankHead.fbx");
	assert(hModel_ >= 0);
}

//更新
void Cannon::Update()
{
	if (Input::IsKey(DIK_RIGHT))
	{
		transform_.rotate_.vecY += SWING_SPEED;
	}

	if (Input::IsKey(DIK_LEFT))
	{
		transform_.rotate_.vecY -= SWING_SPEED;
	}

	//スペースキーを押したら
	if (Input::IsKeyDown(DIK_SPACE))
	{
		Bullet* pBullet = Instantiate<Bullet>(FindObject("PlayScene"));

		XMVECTOR shotPos = Model::GetBonePosition(hModel_, "ShotPoint");
		XMVECTOR cannonRoot = Model::GetBonePosition(hModel_, "CannonRoot");

		pBullet->Shot(shotPos,shotPos - cannonRoot);
	}
}

//描画
void Cannon::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Cannon::Release()
{
}