﻿#include "Tank.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"
#include "Ground.h"
#include "Cannon.h"

//コンストラクタ
Tank::Tank(GameObject * parent)
	:GameObject(parent, "Tank"), hModel_(-1)
{
}

//デストラクタ
Tank::~Tank()
{
}

//初期化
void Tank::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("TankBody.fbx");
	assert(hModel_ >= 0);

	//子供として砲台を追加
	Instantiate<Cannon>(this);
}

//更新
void Tank::Update()
{
	//移動処理
	Move();

	//地面に添わせる
	FitHeightToGround();
}


//戦車移動
void Tank::Move()
{
	XMVECTOR move = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat;
	mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));

	move = XMVector3TransformCoord(move, mat);


	if (Input::IsKey(DIK_W))
	{
		transform_.position_ += move;
	}

	if (Input::IsKey(DIK_S))
	{
		transform_.position_ -= move;
	}

	if (Input::IsKey(DIK_A))
	{
		transform_.rotate_.vecY -= 3;
	}

	if (Input::IsKey(DIK_D))
	{
		transform_.rotate_.vecY += 3;
	}

	//カメラ
	{
		//焦点（見る位置）
		Camera::SetTarget(transform_.position_ + XMVectorSet(0, 4, 0, 0));	//戦車のちょっと上

		//視点（カメラの位置）
		XMVECTOR camVec = { 0, 8, -10, 0 };
		camVec = XMVector3TransformCoord(camVec, mat);
		Camera::SetPosition(transform_.position_ + camVec);
	}
}

//高さを地面に合わせる
void Tank::FitHeightToGround()
{
	Ground* pGround = (Ground*)FindObject("Ground");    //ステージオブジェクトを探す
	int hGroundModel = pGround->GetModelHandle();    //モデル番号を取得

	RayCastData data;
	data.start = transform_.position_;   //レイの発射位置
	data.start.vecY = 0;

	data.dir = XMVectorSet(0, -1, 0, 0); //レイの方向
	Model::RayCast(hGroundModel, &data); //レイを発射

										 //レイが当たったら
	if (data.hit)
	{
		//その分位置を下げる
		transform_.position_.vecY = -data.dist;
	}
}


//描画
void Tank::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Tank::Release()
{
}