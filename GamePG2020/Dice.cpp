﻿#include "Dice.h"


HRESULT Dice::Initialize()
{
	VERTEX  vertices[] =
	{
		//1
		{ XMVectorSet(-1.0f, 1.0f, -1.0f, 0.0f), XMVectorSet(0.0f,  0.0f, 0.0f, 0.0f), XMVectorSet(0.0f,  0.0f, -1.0f, 0.0f) },   // 四角形の頂点（左上）
		{ XMVectorSet(1.0f,  1.0f, -1.0f, 0.0f), XMVectorSet(0.25f, 0.0f, 0.0f, 0.0f), XMVectorSet(0.0f,  0.0f, -1.0f, 0.0f) },   // 四角形の頂点（右上）
		{ XMVectorSet(1.0f, -1.0f, -1.0f, 0.0f), XMVectorSet(0.25f, 0.5f, 0.0f, 0.0f), XMVectorSet(0.0f,  0.0f, -1.0f, 0.0f) },   // 四角形の頂点（右下）
		{ XMVectorSet(-1.0f,-1.0f, -1.0f, 0.0f), XMVectorSet(0.0f,  0.5f, 0.0f, 0.0f), XMVectorSet(0.0f,  0.0f, -1.0f, 0.0f) },   // 四角形の頂点（左下）

		//2
		{ XMVectorSet(1.0f, 1.0f, -1.0f, 0.0f), XMVectorSet(0.25f, 0.0f, 0.0f, 0.0f), XMVectorSet(1.0f,  0.0f, 0.0f, 0.0f) },   // 四角形の頂点（左上）
		{ XMVectorSet(1.0f, 1.0f,  1.0f, 0.0f),	XMVectorSet(0.5f,  0.0f, 0.0f, 0.0f), XMVectorSet(1.0f,  0.0f, 0.0f, 0.0f) },   // 四角形の頂点（右上）
		{ XMVectorSet(1.0f, -1.0f, 1.0f, 0.0f),	XMVectorSet(0.5f,  0.5f, 0.0f, 0.0f), XMVectorSet(1.0f,  0.0f, 0.0f, 0.0f) },   // 四角形の頂点（右下）
		{ XMVectorSet(1.0f,-1.0f, -1.0f, 0.0f), XMVectorSet(0.25f, 0.5f, 0.0f, 0.0f), XMVectorSet(1.0f,  0.0f, 0.0f, 0.0f) },   // 四角形の頂点（左下）

		//3
		{ XMVectorSet(-1.0f, 1.0f, 1.0f, 0.0f), XMVectorSet(0.5f,  0.0f, 0.0f, 0.0f), XMVectorSet(0.0f,  1.0f, 0.0f, 0.0f) },   // 四角形の頂点（左上）
		{ XMVectorSet(1.0f, 1.0f,  1.0f, 0.0f),	XMVectorSet(0.75f, 0.0f, 0.0f, 0.0f), XMVectorSet(0.0f,  1.0f, 0.0f, 0.0f) },   // 四角形の頂点（右上）
		{ XMVectorSet(1.0f, 1.0f, -1.0f, 0.0f),	XMVectorSet(0.75f, 0.5f, 0.0f, 0.0f), XMVectorSet(0.0f,  1.0f, 0.0f, 0.0f) },   // 四角形の頂点（右下）
		{ XMVectorSet(-1.0f,1.0f, -1.0f, 0.0f), XMVectorSet(0.5f,  0.5f, 0.0f, 0.0f), XMVectorSet(0.0f,  1.0f, 0.0f, 0.0f) },   // 四角形の頂点（左下）

		//4
		{ XMVectorSet(1.0f, -1.0f, 1.0f, 0.0f),   XMVectorSet(0.75f,  0.0f, 0.0f, 0.0f), XMVectorSet(1.0f, -1.0f, 0.0f, 0.0f) }, // 四角形の頂点（左上）
		{ XMVectorSet(-1.0f, -1.0f,  1.0f, 0.0f), XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f),   XMVectorSet(1.0f, -1.0f, 0.0f, 0.0f) },   // 四角形の頂点（右上）
		{ XMVectorSet(-1.0f, -1.0f, -1.0f, 0.0f), XMVectorSet(1.0f, 0.5f, 0.0f, 0.0f),   XMVectorSet(1.0f, -1.0f, 0.0f, 0.0f) },   // 四角形の頂点（右下）
		{ XMVectorSet(1.0f,-1.0f, -1.0f, 0.0f),   XMVectorSet(0.75f,  0.5f, 0.0f, 0.0f), XMVectorSet(1.0f, -1.0f, 0.0f, 0.0f) }, // 四角形の頂点（左下）

		//5
		{ XMVectorSet(-1.0f, 1.0f, 1.0f, 0.0f),		XMVectorSet(0.0f,  0.5f, 0.0f, 0.0f) , XMVectorSet(-1.0f,  0.0f, 0.0f, 0.0f)},   // 四角形の頂点（左上）
		{ XMVectorSet(-1.0f, 1.0f,  -1.0f, 0.0f),	XMVectorSet(0.25f, 0.5f, 0.0f, 0.0f) , XMVectorSet(-1.0f,  0.0f, 0.0f, 0.0f)},   // 四角形の頂点（右上）
		{ XMVectorSet(-1.0f, -1.0f, -1.0f, 0.0f),	XMVectorSet(0.25f, 1.0f, 0.0f, 0.0f) , XMVectorSet(-1.0f,  0.0f, 0.0f, 0.0f)},   // 四角形の頂点（右下）
		{ XMVectorSet(-1.0f, -1.0f, 1.0f, 0.0f),	XMVectorSet(0.0f,  1.0f, 0.0f, 0.0f) , XMVectorSet(-1.0f,  0.0f, 0.0f, 0.0f)},   // 四角形の頂点（左下）

		//6
		{ XMVectorSet(1.0f, 1.0f, 1.0f, 0.0f),	XMVectorSet(0.25f,  0.5f, 0.0f, 0.0f), XMVectorSet(0.0f,  0.0f, 1.0f, 0.0f) },   // 四角形の頂点（左上）
		{ XMVectorSet(-1.0f,  1.0f, 1.0f, 0.0f), XMVectorSet(0.5f,  0.5f, 0.0f, 0.0f), XMVectorSet(0.0f,  0.0f, 1.0f, 0.0f) },   // 四角形の頂点（右上）
		{ XMVectorSet(-1.0f, -1.0f, 1.0f, 0.0f), XMVectorSet(0.5f,  1.0f, 0.0f, 0.0f), XMVectorSet(0.0f,  0.0f, 1.0f, 0.0f) },   // 四角形の頂点（右下）
		{ XMVectorSet(1.0f,-1.0f, 1.0f, 0.0f),	XMVectorSet(0.25f,  1.0f, 0.0f, 0.0f), XMVectorSet(0.0f,  0.0f, 1.0f, 0.0f) },   // 四角形の頂点（左下）
	};

	// 頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;
	bd_vertex.ByteWidth = sizeof(vertices);
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = 0;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = vertices;
	Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);


	int index[] = { 0,2,3,  0,1,2   ,4,6,7,  4, 5, 6    ,8,10,11,  8, 9, 10   ,12,14,15,  12, 13, 14   ,16,18,19,  16, 17, 18  ,20,22,23,  20, 21, 22 };

	// インデックスバッファを生成する
	D3D11_BUFFER_DESC   bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(index);
	bd.BindFlags = D3D10_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = index;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;
	Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);

	indexNum = sizeof(index) / sizeof(int);

	//コンスタントバッファ作成
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	// 定数バッファの作成
	Direct3D::pDevice->CreateBuffer(&cb, NULL, &pConstantBuffer_);


	pTexture_ = new Texture;
	pTexture_->Load("Assets\\dice.png");

	return S_OK;
}
