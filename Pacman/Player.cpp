﻿#include "Player.h"
#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/Input.h"

//コンストラクタ
Player::Player(GameObject * parent)
	:GameObject(parent, "Player"), hModel_(-1)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	hModel_ = Model::Load("Player.fbx");
	assert(hModel_ >= 0);

	transform_.position_.vecX = 5;
	transform_.position_.vecZ = 5;

	pStage_ = (Stage*)FindObject("Stage");
	assert(pStage_ != nullptr);
}

//更新
void Player::Update()
{
	//移動前の位置
	XMVECTOR prevPosition = transform_.position_;

	XMVECTOR move = { 0, 0, 0, 0 };

	if (Input::IsKey(DIK_RIGHT) || Input::IsPadButton(XINPUT_GAMEPAD_DPAD_RIGHT))
	{
		move.vecX = 1;
	}

	if (Input::IsKey(DIK_LEFT) || Input::IsPadButton(XINPUT_GAMEPAD_DPAD_LEFT))
	{
		move.vecX = -1;
	}

	if (Input::IsKey(DIK_UP) || Input::IsPadButton(XINPUT_GAMEPAD_DPAD_UP))
	{
		move.vecZ = 1;
	}

	if (Input::IsKey(DIK_DOWN) || Input::IsPadButton(XINPUT_GAMEPAD_DPAD_DOWN))
	{
		move.vecZ = -1;
	}

	move.vecX = Input::GetPadStickL().vecX;
	move.vecZ = Input::GetPadStickL().vecY;

	if (XMVector3Length(move).vecX > 0)
	{
		transform_.position_ += move * 0.1f;
		move = XMVector3Normalize(move);

		XMVECTOR front = { 0, 0, 1, 0 };
		float dot = XMVector3Dot(front, move).vecX;
		float angle = acos(dot);

		XMVECTOR cross = XMVector3Cross(front, move);
		if (cross.vecY < 0)
		{
			angle *= -1;
		}

		transform_.rotate_.vecY = XMConvertToDegrees(angle);
	}

	int checkX, checkZ;

	//左側の判定
	checkX = (int)(transform_.position_.vecX - 0.3f);
	checkZ = (int)transform_.position_.vecZ;
	if (pStage_->IsWall(checkX, checkZ))
	{
		transform_.position_.vecX = checkX + 1 + 0.3f;
	}

	//右側の判定
	checkX = (int)(transform_.position_.vecX + 0.3f);
	checkZ = (int)transform_.position_.vecZ;
	if (pStage_->IsWall(checkX, checkZ))
	{
		transform_.position_.vecX = checkX - 0.3;
	}

	//奥側の判定
	checkX = (int)transform_.position_.vecX;
	checkZ = (int)(transform_.position_.vecZ + 0.3f);
	if (pStage_->IsWall(checkX, checkZ))
	{
		transform_.position_.vecZ = checkZ - 0.3f;
	}

	//手前側の判定
	checkX = (int)transform_.position_.vecX;
	checkZ = (int)(transform_.position_.vecZ - 0.3f);
	if (pStage_->IsWall(checkX, checkZ))
	{
		transform_.position_.vecZ = checkZ + 1 + 0.3f;
	}

}

//描画
void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}