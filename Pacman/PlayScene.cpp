﻿#include "PlayScene.h"
#include "Stage.h"
#include "Player.h"
#include "Engine/Camera.h"

//コンストラクタ
PlayScene::PlayScene(GameObject * parent)
	: GameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	Instantiate<Stage>(this);
	Instantiate<Player>(this);

	Camera::SetPosition(XMVectorSet(6.5, 7, -7, 0));
	Camera::SetTarget(XMVectorSet(6.5, 0, 6.5, 0));
}

//更新
void PlayScene::Update()
{
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}
