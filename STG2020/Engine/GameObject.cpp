#include "GameObject.h"
#include "..\Collider.h"

GameObject::GameObject():GameObject(nullptr, "")
{
}

GameObject::GameObject(GameObject * parent, const std::string & name) :
	pParent_(parent), objectName_(name), isDead_(false), _pCollider(nullptr)
{
	childList_.clear();
}

GameObject::~GameObject()
{
	AllChildKill();
}

void GameObject::UpdateSub()
{
	Update();
	transform_.Calclation();

	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->UpdateSub();
	}

	for (auto itr = childList_.begin(); itr != childList_.end(); )
	{
		if ((*itr)->isDead_ == true)
		{
			(*itr)->ReleaseSub();
			delete(*itr);
			itr = childList_.erase(itr);
		}
		else
		{
			(*itr)->Collision(pParent_);
			itr++;
		}

	}


}

void GameObject::DrawSub()
{
	Draw();

	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->DrawSub();
	}
}

void GameObject::ReleaseSub()
{
	Release();

	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->ReleaseSub();
	}
}

GameObject * GameObject::FindObject(std::string name)
{
	return GetRootJob()->FindChildObject(name);
}

GameObject * GameObject::FindChildObject(std::string name)
{
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		if ((*itr)->objectName_ == name)
		{
			return *itr;
		}

		GameObject* pObj = (*itr)->FindChildObject(name);
		if (pObj != nullptr)
		{
			return pObj;
		}
	}

	return nullptr;
}

GameObject * GameObject::GetRootJob()
{
	if (pParent_ == nullptr)
	{
		return this;
	}
	else
	{
		return pParent_->GetRootJob();
	}
}

void GameObject::AllChildKill()
{
	for (auto itr = childList_.begin();	itr != childList_.end(); itr++)
	{
		(*itr)->Release();
		delete(*itr);
	}
	childList_.clear();
}

void GameObject::AddCollider(Collider * collider)
{
	_pCollider = collider;
	_pCollider->SetMaster(this);
}

void GameObject::Collision(GameObject * pTarget)
{
	if (pTarget == nullptr)
	{
		return;
	}

	if (_pCollider != nullptr && pTarget->_pCollider != nullptr && pTarget != this)
	{
		if (_pCollider->Collision(pTarget->_pCollider))
		{
			OnCollision(pTarget);
		}
	}

	for (auto i = pTarget->childList_.begin(); i != pTarget->childList_.end(); i++)
	{
		Collision(*i);
	}
}


