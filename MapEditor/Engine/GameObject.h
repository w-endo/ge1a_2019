#pragma once

#include <string>
#include <list>
#include "Transform.h"

class Collider;

class GameObject
{
	std::string		objectName_;
	bool			isDead_;

protected:
	GameObject*		pParent_;
	std::list<GameObject*> childList_;

public:
	Transform		transform_;

public:
	GameObject();
	GameObject(GameObject* parent, const std::string& name);
	virtual ~GameObject();

	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;
	virtual void Release() = 0;

	void UpdateSub();
	void DrawSub();
	void ReleaseSub();

	void KillMe() { isDead_ = true; }

	GameObject* FindObject(std::string name);
	GameObject* FindChildObject(std::string name);
	GameObject* GetRootJob();

	void AllChildKill();

	Collider* _pCollider;
	void AddCollider(Collider* collider);

	void Collision(GameObject* pTarget);

	virtual void OnCollision(GameObject *pTarget) {};

	template <class T>
	GameObject* Instantiate(GameObject* parent)
	{
		T* p;
		p = new T(parent);
		p->Initialize();
		childList_.push_back(p);

		p->transform_.pParent_ = &parent->transform_;
		return p;
	}
};