//───────────────────────────────────────
// テクスチャ＆サンプラーデータのグローバル変数定義
//───────────────────────────────────────
Texture2D		g_texture : register(t0);	//テクスチャー
SamplerState	g_sampler : register(s0);	//サンプラー
Texture2D		g_normalTexture : register(t1);	//テクスチャー
TextureCube		g_cubeTexture : register(t2);	//キューブテクスチャー
//───────────────────────────────────────
// コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
//───────────────────────────────────────
cbuffer global
{
	float4x4	matWVP;	// ワールド・ビュー・プロジェクションの合成行列
	float4x4	matNormal;	//回転×拡大の逆行列
	float4x4	matWorld;	//ワールド行列
	float4		camPos;	//カメラの位置

	float4		diffuseColor;	// 拡散反射光
	float4		ambientColor;	// 環境光
	float4		specularColor;	// 鏡面反射光
	float		shininess;		// 光沢度
	bool		isTexture;		// テクスチャ貼ってあるかどうか

	float	    scroll;
};

struct VS_OUT
{
	float4 pos : SV_POSITION;
//	float4 normal : NORMAL;
	float4 eye : TEXCOORD1;
	float2 uv	: TEXCOORD;		//UV座標
	float4 light : TEXCOORD2;
};


//頂点シェーダー
VS_OUT VS(float4 pos : POSITION, float4 uv : TEXCOORD, float4 normal : NORMAL, float4 tangent : TANGENT)
{
	VS_OUT outData;

	outData.pos = mul(pos, matWVP);

	outData.uv = uv;

	float3 binormal = cross(normal, tangent);

	normal.w = 0;
	tangent.w = 0;

	normal = mul(normal, matNormal);
	tangent = mul(tangent, matNormal);
	binormal = mul(binormal, matNormal);

	float4 light = float4(1, -1, -1, 0);
	light = normalize(light);
	outData.light.x = dot(light, tangent);
	outData.light.y = dot(light, binormal);
	outData.light.z = dot(light, normal);


	float4 eye = normalize(camPos - mul(pos, matWorld));		//視点へのベクトル
	outData.eye.x = dot(eye, tangent);
	outData.eye.y = dot(eye, binormal);
	outData.eye.z = dot(eye, normal);

	return outData;
}

//ピクセルシェーダー
float4 PS(VS_OUT inData) : SV_TARGET
{
	
	inData.light = normalize(inData.light);

	float2 normalUV_A = float2(inData.uv.x + scroll*0.3, inData.uv.y);
	float4 normal_A = g_normalTexture.Sample(g_sampler, normalUV_A) * 2 - 1;
	normal_A.w = 0;

	float2 normalUV_B = float2(inData.uv.x*0.7, inData.uv.y*-0.7 - scroll * 0.5);
	float4 normal_B = g_normalTexture.Sample(g_sampler, normalUV_B) * 2 - 1;
	normal_B.w = 0;

	float4 normal = normalize(normal_A + normal_B + float4(0, 10, 0, 0));

	//normal = float4(0, 1, 0, 0);


	float4 ambient = ambientColor;	//環境光（kaia)
	float4 LN = saturate(dot(-inData.light, normalize(normal)));		//明るさ
	float4 id;
	if (isTexture == true)
	{
		id = g_texture.Sample(g_sampler, inData.uv);
	}
	else
	{
		id = diffuseColor;			//色
	}
	float4 diffuse = LN * id;	//拡散反射光
	float4 R = reflect(inData.light, normalize(normal));		//反射ベクトル

//	float shininess = 5;					//光沢度
	float4 is = specularColor;			//ハイライトの色
	float ks = 2;
	float4 specular = ks * pow(saturate(dot(R, normalize(inData.eye))), shininess) * is;


	float3 eyeRef = -reflect(normalize(inData.eye), normalize(normal));
	float4 envColor = g_cubeTexture.Sample(g_sampler, eyeRef);
	diffuse = diffuse * 0.0 + envColor * 1.0;


	float4 color = ambient + diffuse + specular;

	color.a = 0.5;
	return color;
}