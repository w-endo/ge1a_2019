﻿#pragma once
#include <d3d11.h>

//リンカ
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}

enum SHADER_TYPE { SHADER_3D, SHADER_2D, SHADER_MAX };

namespace Direct3D
{
	extern ID3D11Device*		pDevice;
	extern ID3D11DeviceContext* pContext;
	extern int scrWidth;
	extern int scrHeight;

	//初期化
	//引数：winW　ウィンドウ幅
	//引数：winH　ウィンドウ高さ
	//引数：hWnd　ウィンドウハンドル
	//戻値：なし
	void Initialize(int winW, int winH, HWND hWnd);

	//シェーダー準備
	void InitShader();

	void SetShaderBundle(SHADER_TYPE type);

	void InitShader3D();
	void InitShader2D();

	//描画開始
	void BeginDraw();

	//描画終了
	void EndDraw();

	//解放
	void Release();
};