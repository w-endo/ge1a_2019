#pragma once

#include <string>
#include <vector>
#include "Direct3D.h"
#include "Transform.h"
#include "Fbx.h"



namespace Model
{
	struct ModelData
	{
		Transform transform;
		std::string fileName;
		SHADER_TYPE shaderType;
		Fbx* pFbx;

		ModelData() : pFbx(nullptr)
		{
		}
	};

	int Load(std::string fileName, SHADER_TYPE shaderType = SHADER_TEST);
	void SetTransform(int handle, Transform& transform);
	void Draw(int handle);

	void RayCast(int handle, RayCastData *rayData);
};

