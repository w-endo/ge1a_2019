#include "Collider.h"
#include "Transform.h"
#include "GameObject.h"

Collider::Collider(XMVECTOR center, float radius):
	center_(center), radius_(radius)
{
}

bool Collider::Collision(Collider * pTarget)
{
	XMVECTOR v = pMaster_->transform_.position_ + center_ 
		- pTarget->pMaster_->transform_.position_ + pTarget->center_;
	float l = XMVector3Length(v).vecX;
	if (l < radius_ + pTarget->radius_)
	{
		return true;
	}
	return false;
}
