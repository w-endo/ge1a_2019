#include "Map.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Direct3D.h"
#include "Engine/Camera.h"
#include "resource.h"

//コンストラクタ
Map::Map(GameObject * parent)
	:GameObject(parent, "Map"), mode_(0), type_(TYPE_DEFAULT)
{
	for (int i = 0; i < TYPE_MAX; i++)
	{
		hModel_[i] = -1;
	}


	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			table_[x][z].type = TYPE_DEFAULT;
			table_[x][z].height = 1;
		}
	}
}

//デストラクタ
Map::~Map()
{
}

//初期化
void Map::Initialize()
{
	//モデルデータのロード
	hModel_[TYPE_DEFAULT] = Model::Load("Assets/BoxDefault.fbx");
	assert(hModel_[TYPE_DEFAULT] >= 0);

	hModel_[TYPE_BRICK] = Model::Load("Assets/BoxBrick.fbx");
	hModel_[TYPE_GRASS] = Model::Load("Assets/BoxGrass.fbx");
	hModel_[TYPE_SAND] = Model::Load("Assets/BoxSand.fbx");
	hModel_[TYPE_WATER] = Model::Load("Assets/BoxWater.fbx");
}



//更新
void Map::Update()
{
	//マウスをクリックしたら
	if (Input::IsMouseButtonDown(0))
	{
		//ビューポート行列
		float w = Direct3D::scrWidth / 2.0f;
		float h = Direct3D::scrHeight / 2.0f;
		XMMATRIX vp = {
			w, 0, 0, 0,
			0, -h, 0, 0,
			0, 0, 1, 0,
			w, h, 0, 1
		};


		//各逆行列
		XMMATRIX invVP = XMMatrixInverse(nullptr, vp);
		XMMATRIX invPrj = XMMatrixInverse(nullptr, Camera::GetProjectionMatrix());
		XMMATRIX invView = XMMatrixInverse(nullptr, Camera::GetViewMatrix());


		//クリック位置（手前）
		XMVECTOR mousePosFront = Input::GetMousePosition();
		mousePosFront.vecZ = 0.0f;

		//クリック位置（奥）
		XMVECTOR mousePosBack = Input::GetMousePosition();
		mousePosBack.vecZ = 1.0f;


		//変換
		mousePosFront = XMVector3TransformCoord(mousePosFront, invVP * invPrj* invView);
		mousePosBack = XMVector3TransformCoord(mousePosBack, invVP * invPrj* invView);


		
		int nearX;
		int nearZ;
		float miniDist = 9999;
		float isHit = false;

		for (int x = 0; x < 15; x++)
		{
			for (int z = 0; z < 15; z++)
			{
				for (int y = 0; y < table_[x][z].height; y++)
				{
					Transform trans;
					trans.position_.vecX = x;
					trans.position_.vecY = y;
					trans.position_.vecZ = z;
					trans.Calclation();

					//レイキャスト
					RayCastData data;
					data.start = mousePosFront;
					data.dir = mousePosBack - mousePosFront;
					Model::SetTransform(hModel_[0], trans);
					Model::RayCast(hModel_[0], &data);

					//当たったかテスト
					if (data.hit)
					{
						isHit = true;
						if (data.dist < miniDist)
						{
							miniDist = data.dist;
							nearX = x;
							nearZ = z;
						}
					}
				}
			}
		}
		if (isHit)
		{
			switch (mode_)
			{
			case 0:
				table_[nearX][nearZ].height++;
				break;

			case 1:
				table_[nearX][nearZ].height--;
				if (table_[nearX][nearZ].height < 1)	table_[nearX][nearZ].height = 1;
				break;

			case 2:
				table_[nearX][nearZ].type = type_;
				break;

			}
		}
	}
}

//描画
void Map::Draw()
{
	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			int type = table_[x][z].type;

			for (int y = 0; y < table_[x][z].height; y++)
			{
				Transform trans;
				trans.position_.vecX = x;
				trans.position_.vecY = y;
				trans.position_.vecZ = z;
				trans.Calclation();


				Model::SetTransform(hModel_[type], trans);
				Model::Draw(hModel_[type]);
			}
		}
	}
}

//開放
void Map::Release()
{
}


//ニセモノ
BOOL Map::DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	switch (LOWORD(wp))
	{
	case IDC_RADIO_UP:
			mode_ = 0;
			return TRUE;

	case IDC_RADIO_DOWN:
			mode_ = 1;
			return TRUE;

	case IDC_RADIO_CHANGE:
			mode_ = 2;
			return TRUE;

	case IDC_COMBO:
		type_ = (Type)SendMessage(GetDlgItem(hDlg, IDC_COMBO), CB_GETCURSEL, 0, 0);
		return TRUE;

	}
	return FALSE;
}

void Map::Save()
{
	HANDLE hFile;        //ファイルのハンドル
	hFile = CreateFile(
		"test.txt",                 //ファイル名
		GENERIC_WRITE,           //アクセスモード（書き込み用）
		0,                      //共有（なし）
		NULL,                   //セキュリティ属性（継承しない）
		CREATE_ALWAYS,           //作成方法
		FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
		NULL);                  //拡張属性（なし）


	std::string str = "";
	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			str += std::to_string( table_[x][z].height);
			str += ",";
			str += std::to_string(table_[x][z].type);
			str += ",";
		}
	}



	DWORD dwBytes = 0;  //書き込み位置
	WriteFile(
		hFile,                   //ファイルハンドル
		str.c_str(),                  //保存するデータ（文字列）
		(DWORD)strlen(str.c_str()),   //書き込む文字数
		&dwBytes,                //書き込んだサイズを入れる変数
		NULL);                   //オーバーラップド構造体（今回は使わない）

	CloseHandle(hFile);
}

void Map::Load()
{
	HANDLE hFile;        //ファイルのハンドル
	hFile = CreateFile(
		"test.txt",                 //ファイル名
		GENERIC_READ,           //アクセスモード（書き込み用）
		0,                      //共有（なし）
		NULL,                   //セキュリティ属性（継承しない）
		OPEN_EXISTING,           //作成方法
		FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
		NULL);                  //拡張属性（なし）

	  //ファイルのサイズを取得
	DWORD fileSize = GetFileSize(hFile, NULL);

	//ファイルのサイズ分メモリを確保
	char* data;
	data = new char[fileSize];

	DWORD dwBytes = 0; //読み込み位置

	ReadFile(
		hFile,     //ファイルハンドル
		data,      //データを入れる変数
		fileSize,  //読み込むサイズ
		&dwBytes,  //読み込んだサイズ
		NULL);     //オーバーラップド構造体（今回は使わない）

	CloseHandle(hFile);

	int index = 0;


	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			table_[x][z].height = GetToComma(data, &index);
			table_[x][z].type = (Type)GetToComma(data, &index);
		}
	}

	delete[] data;
}

int Map::GetToComma(char * str, int * index)
{
	std::string buf = "";

	while (true)
	{
		if (str[*index] == ',')
		{
			break;
		}
		buf += str[*index];
		(*index)++;
	}
	(*index)++;
	return atoi(buf.c_str());
}


