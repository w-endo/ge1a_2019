#include <xaudio2.h>
#include <vector>
#include "Audio.h"

#define SAFE_DELETE_ARRAY(p) if(p){delete[] p; p = nullptr;}

namespace Audio
{
	IXAudio2* pXAudio = nullptr;
	IXAudio2MasteringVoice* pMasteringVoice = nullptr;

	struct AudioData
	{
		XAUDIO2_BUFFER buf = {};
		IXAudio2SourceVoice** pSourceVoice = nullptr;
		int svNum;
	};
	std::vector<AudioData>	audioDatas;
}

void Audio::Initialize()
{
	CoInitializeEx(0, COINIT_MULTITHREADED);

	XAudio2Create(&pXAudio);
	pXAudio->CreateMasteringVoice(&pMasteringVoice);

}

int Audio::Load(std::string fileName, int svNum)
{
	struct Chunk
	{
		char	id[4]; 		// ID
		unsigned int	size;	// �T�C�Y
	};

	HANDLE hFile;
	hFile = CreateFile(fileName.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	DWORD dwBytes = 0;

	Chunk riffChunk;
	ReadFile(hFile, &riffChunk, 8, &dwBytes, NULL);

	char wave[4];
	ReadFile(hFile, &wave, 4, &dwBytes, NULL);

	Chunk formatChunk;
	ReadFile(hFile, &formatChunk, 8, &dwBytes, NULL);

	WAVEFORMATEX	fmt;
	ReadFile(hFile, &fmt, formatChunk.size, &dwBytes, NULL);

	Chunk data;
	ReadFile(hFile, &data, 8, &dwBytes, NULL);

	char* pBuffer = new char[data.size];
	ReadFile(hFile, pBuffer, data.size, &dwBytes, NULL);

	CloseHandle(hFile);


	AudioData ad;

	ad.buf.pAudioData = (BYTE*)pBuffer;
	ad.buf.Flags = XAUDIO2_END_OF_STREAM;
	ad.buf.AudioBytes = data.size;

	ad.pSourceVoice = new IXAudio2SourceVoice*[svNum];
	for (int i = 0; i < svNum; i++)
	{
		pXAudio->CreateSourceVoice(&ad.pSourceVoice[i], &fmt);
	}
	ad.svNum = svNum;
	audioDatas.push_back(ad);

	SAFE_DELETE_ARRAY(pBuffer);

	return audioDatas.size() - 1;
}

void Audio::Play(int ID)
{
	for (int i = 0; i < audioDatas[ID].svNum; i++)
	{
		XAUDIO2_VOICE_STATE state;
		audioDatas[ID].pSourceVoice[i]->GetState(&state);

		if (state.BuffersQueued == 0)
		{
			audioDatas[ID].pSourceVoice[i]->SubmitSourceBuffer(&audioDatas[ID].buf);
			audioDatas[ID].pSourceVoice[i]->Start();
			break;
		}
	}
}

void Audio::Release()
{
	for (int i = 0; i < audioDatas.size; i++)
	{
		for (int j = 0; j < audioDatas[i].svNum; j++)
		{
			audioDatas[i].pSourceVoice[j]->DestroyVoice();
		}
	}

	CoUninitialize();
	pMasteringVoice->DestroyVoice();
	pXAudio->Release();
}
