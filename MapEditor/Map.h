#pragma once
#include <Windows.h>
#include "Engine/GameObject.h"

//マップを管理するクラス
class Map : public GameObject
{
	enum Type
	{
		TYPE_DEFAULT,
		TYPE_BRICK,
		TYPE_GRASS,
		TYPE_SAND,
		TYPE_WATER,
		TYPE_MAX
	};

	struct 
	{
		int height;
		Type type;
	}table_[15][15];


	int hModel_[TYPE_MAX];    //モデル番号


	int mode_;
	Type type_;



public:
	//コンストラクタ
	Map(GameObject* parent);

	//デストラクタ
	~Map();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	BOOL DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);

	void Save();

	void Load();

	int GetToComma(char* str, int* index);
};